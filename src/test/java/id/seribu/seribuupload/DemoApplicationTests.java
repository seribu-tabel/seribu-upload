package id.seribu.seribuupload;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import id.seribu.seribuupload.config.FileUploadConfig;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FileUploadConfig.class})
public class DemoApplicationTests {

	@Autowired
	FileUploadConfig fileUploadConfig;

	@Test
	public void contextLoads() {
	}


	@Test
	public void testPathFile() {
		System.out.println(fileUploadConfig.getUploadDir());
	}
}
