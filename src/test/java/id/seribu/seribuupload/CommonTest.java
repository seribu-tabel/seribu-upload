package id.seribu.seribuupload;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

public class CommonTest {

    @Test
    public void generateYearTest() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Assert.assertEquals(2019, year);
    }
}