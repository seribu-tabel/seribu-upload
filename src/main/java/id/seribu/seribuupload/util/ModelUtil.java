package id.seribu.seribuupload.util;

import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Component;

import id.seribu.seribudto.transaction.UploadRequestDto;
import id.seribu.seribumodel.transaction.TPihakLain;
import id.seribu.seribumodel.transaction.TSubdit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ModelUtil {

    public TSubdit createModelSubdit(UploadRequestDto dto, String fileName, String fileType, int year, String description) {
        log.info("ModelUtil.createSubditModelSubdit Entity:[{}], filename: {}", dto.toString(), fileName);
        TSubdit tSubdit = new TSubdit();
        tSubdit.setId(UUID.randomUUID());
        tSubdit.setSubditName(dto.getOwner());
        tSubdit.setPath(dto.getDirectory());
        tSubdit.setFileName(fileName);
        tSubdit.setUploadedBy(dto.getUploadedBy());
        tSubdit.setCreatedDate(new Date());
        tSubdit.setEnabled(true);
        tSubdit.setFileType(fileType);
        tSubdit.setReleaseYear(year);
        tSubdit.setDescription(description);
        return tSubdit;
    }

    public TPihakLain createModelPihakLain(UploadRequestDto dto, String fileName, String fileType, int year, String description) {
        log.info("ModelUtil.createSubditModelSubdit Entity:[{}], filename: {}", dto.toString(), fileName);
        TPihakLain tPihakLain = new TPihakLain();
        tPihakLain.setId(UUID.randomUUID());
        tPihakLain.setSubditName(dto.getOwner());
        tPihakLain.setPath(dto.getDirectory());
        tPihakLain.setFileName(fileName);
        tPihakLain.setUploadedBy(dto.getUploadedBy());
        tPihakLain.setCreatedDate(new Date());
        tPihakLain.setEnabled(true);
        tPihakLain.setFileType(fileType);
        tPihakLain.setReleaseYear(year);
        tPihakLain.setDescription(description);
        return tPihakLain;
    }

    public TSubdit updateModelSubdit(TSubdit oldData, String uploadedBy) {
        log.info("ModelUtil.updateModelSubdit ID: [{}]", oldData.getId());
        TSubdit tSubdit = new TSubdit();
        try {
            tSubdit.setId(oldData.getId());
            tSubdit.setFileName(oldData.getFileName());
            tSubdit.setPath(oldData.getPath());
            tSubdit.setFileType(oldData.getFileType());
            tSubdit.setSubditName(oldData.getSubditName());
            tSubdit.setCreatedDate(oldData.getCreatedDate());
            tSubdit.setUploadedBy(uploadedBy);
            tSubdit.setUpdatedDate(new Date());
            tSubdit.setEnabled(true);
        } catch (Exception e) {
            log.error("fail ModelUtil.updateModelSubdit, filename {}", oldData.getFileName(), e);
        }
        return tSubdit;
    }

    public TPihakLain updateModelPihakLain(TPihakLain oldData, String uploadedBy) {
        log.info("ModelUtil.updateModelSubdit ID: [{}]", oldData.getId());
        TPihakLain tPihakLain = new TPihakLain();
        try {
            tPihakLain.setId(oldData.getId());
            tPihakLain.setFileName(oldData.getFileName());
            tPihakLain.setPath(oldData.getPath());
            tPihakLain.setFileType(oldData.getFileType());
            tPihakLain.setSubditName(oldData.getSubditName());
            tPihakLain.setCreatedDate(oldData.getCreatedDate());
            tPihakLain.setUploadedBy(uploadedBy);
            tPihakLain.setUpdatedDate(new Date());
            tPihakLain.setEnabled(true);
        } catch (Exception e) {
            log.error("fail ModelUtil.updateModelSubdit, filename {}", oldData.getFileName(), e);
        }
        return tPihakLain;
    }
}