package id.seribu.seribuupload.util;

import org.springframework.stereotype.Component;

import id.seribu.seribudto.transaction.UploadResponseDto;
import id.seribu.seribumodel.transaction.TPihakLain;
import id.seribu.seribumodel.transaction.TSubdit;
import id.seribu.seribuupload.constant.SeribuUploadConstant;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DtoUtil {

    public UploadResponseDto createDtoPihakLain(TPihakLain tPihakLain) {
        log.info("DtoUtil.createDtoPihakLain");
        return new UploadResponseDto(tPihakLain.getId(), SeribuUploadConstant.PIHAK_LAIN, tPihakLain.getSubditName(),
                tPihakLain.getFileName(), tPihakLain.getPath(), tPihakLain.getFileType(), new Long(0),
                String.valueOf(tPihakLain.getReleaseYear()), tPihakLain.getDescription());
    }

    public UploadResponseDto createdDtoSubdit(TSubdit tSubdit) {
        log.info("DtoUtil.createdDtoSubdit");
        return new UploadResponseDto(tSubdit.getId(), SeribuUploadConstant.SUBDIT, tSubdit.getSubditName(), tSubdit.getFileName(),
            tSubdit.getPath(), tSubdit.getFileType(),
                new Long(0), String.valueOf(tSubdit.getReleaseYear()), tSubdit.getDescription());
    }
}