package id.seribu.seribuupload.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.UploadRequestDto;
import id.seribu.seribudto.transaction.UploadResponseDto;

public interface PihaklainService {

    ApiResponse<UploadResponseDto> processFile(MultipartFile file, String owner, String uploadedBy, int year, String description);
    DownloadResponseDto download(String filename, String owner, int year);
    List<UploadResponseDto> findAllActiveFile();
    int getTotalPihakLain();
    boolean deleteDataAndFile(UploadRequestDto uploadRequestDto);

}