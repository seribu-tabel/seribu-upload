package id.seribu.seribuupload.service;

import java.util.List;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribudto.template.ApiResponse;

public interface SysParamService {

    ApiResponse<List<Lov>> listSysParam(String groupParam);
}