package id.seribu.seribuupload.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.UploadRequestDto;
import id.seribu.seribudto.transaction.UploadResponseDto;
import id.seribu.seribumodel.transaction.TSubdit;
import id.seribu.seribuupload.config.FileUploadConfig;
import id.seribu.seribuupload.constant.SeribuUploadConstant;
import id.seribu.seribuupload.repository.SubditRepository;
import id.seribu.seribuupload.service.AbstractProcessFile;
import id.seribu.seribuupload.service.SubditService;
import id.seribu.seribuupload.util.DtoUtil;
import id.seribu.seribuupload.util.ModelUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SubditServiceImpl extends AbstractProcessFile implements SubditService {

    @Autowired
    SubditRepository subditRepository;

    @Autowired
    ModelUtil modelUtil;

    @Autowired
    DtoUtil dtoUtil;

    @Autowired
    FileUploadConfig fileUploadConfig;

    @Transactional
    @Override
    public ApiResponse<UploadResponseDto> processFile(MultipartFile file, String owner, String uploadedBy, int year,
            String description) {
        log.info("SubditService.processFile check exist file");
        TSubdit tSubdit = subditRepository.findByFileNameAndSubditYear(owner, file.getOriginalFilename(), year);
        // check file exist
        if (tSubdit != null) {
            log.info("SubditService.processFile update exist data, ID: {}", tSubdit.getId());
            TSubdit editedData = new TSubdit();
            editedData = modelUtil.updateModelSubdit(tSubdit, uploadedBy);
            subditRepository.save(editedData);
        } else {
            log.info("SubditService.processFile new data, filename: {}", file.getOriginalFilename());
            subditRepository
                    .save(modelUtil.createModelSubdit(this.createRequestDto(owner, uploadedBy, this.generateDir(owner), year, description),
                            file.getOriginalFilename(), file.getContentType(), year, description));
        }
        UploadResponseDto response = this.upload(file,
                this.createRequestDto(owner, uploadedBy, this.generateDir(owner), year, description));

        return new ApiResponse<>(HttpStatus.OK.value(), "File berhasil diupload", response);
    }

    private UploadRequestDto createRequestDto(String owner, String uploadedBy, String directory, Integer releaseYear, String description) {
        return new UploadRequestDto(UUID.randomUUID(), SeribuUploadConstant.SUBDIT, owner, directory, uploadedBy, releaseYear, description);
    }

    private String generateDir(String owner) {
        log.info("SubditService.generateDir owner name: [{}]", owner);
        String path = fileUploadConfig.getUploadDir() + SeribuUploadConstant.Directory.DIR_SUBDIT
                + SeribuUploadConstant.Directory.PATH_PREFIX + owner + SeribuUploadConstant.Directory.PATH_PREFIX
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        return path;
    }

    @Override
    public DownloadResponseDto download(String fileName, String owner, int year) {
        log.info("SubditService.download [subditName: {}, filename: {}, releaseYear: {}]", owner, fileName, year);
        TSubdit tSubdit = subditRepository.findByFileNameAndSubditYear(owner, fileName, year);
        return this.downloadFile(this.getPath(tSubdit), tSubdit.getFileType());
    }

    private String getPath(TSubdit tSubdit) {
        log.info("path: {}", tSubdit.getPath() + SeribuUploadConstant.Directory.PATH_PREFIX + tSubdit.getFileName());
        return tSubdit.getPath() + SeribuUploadConstant.Directory.PATH_PREFIX + tSubdit.getFileName();
    }

    @Override
    public List<UploadResponseDto> findAllActiveFile() {
        log.info("SubditService.findAllActiveFile");
        List<TSubdit> listActiveFile = subditRepository.findAllbyStatus();
        return listActiveFile.stream()
                .map(u -> new UploadResponseDto(u.getId(), SeribuUploadConstant.SUBDIT, u.getSubditName(), u.getFileName(),
                        u.getPath(), u.getFileType(), 0, String.valueOf(u.getReleaseYear()), u.getDescription()))
                .collect(Collectors.toList());
    }

    @Override
    public int getTotalSubdit() {
        log.info("SubditService.getTotalSubdit");
        return subditRepository.findAllbyStatus().size();
    }

    @Transactional
    @Override
    public boolean deleteDataAndFile(UploadRequestDto uploadRequestDto) {
        log.info("SubditService.deleteDataAndFile ID:{}", uploadRequestDto.getId());
        boolean deleted = false;
        Optional<TSubdit> opSubdit = subditRepository.findById(uploadRequestDto.getId());
        subditRepository.delete(opSubdit.get());
        deleted = this.removeFile(opSubdit.get().getPath().concat(SeribuUploadConstant.Directory.PATH_PREFIX).concat(opSubdit.get().getFileName()));
        return deleted;
    }

}