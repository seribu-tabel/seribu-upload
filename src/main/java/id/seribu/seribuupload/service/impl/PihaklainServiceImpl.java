package id.seribu.seribuupload.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.UploadRequestDto;
import id.seribu.seribudto.transaction.UploadResponseDto;
import id.seribu.seribumodel.transaction.TPihakLain;
import id.seribu.seribuupload.config.FileUploadConfig;
import id.seribu.seribuupload.constant.SeribuUploadConstant;
import id.seribu.seribuupload.repository.PihaklainRepository;
import id.seribu.seribuupload.service.AbstractProcessFile;
import id.seribu.seribuupload.service.PihaklainService;
import id.seribu.seribuupload.util.DtoUtil;
import id.seribu.seribuupload.util.ModelUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PihaklainServiceImpl extends AbstractProcessFile implements PihaklainService {

    @Autowired
    PihaklainRepository pihaklainRepository;

    @Autowired
    ModelUtil modelUtil;

    @Autowired
    DtoUtil dtoUtil;

    @Autowired
    FileUploadConfig fileUploadConfig;

    @Transactional
    @Override
    public ApiResponse<UploadResponseDto> processFile(MultipartFile file, String owner, String uploadedBy, int year,
            String description) {
        log.info("PihaklainService.processFile check exist file");
        TPihakLain tPihakLain = pihaklainRepository.findByFileNameAndSubditYear(owner, file.getOriginalFilename(),
                year);
        // check exist file
        if (tPihakLain != null) {
            log.info("PihaklainService.processFile update exist data, ID: {}", tPihakLain.getId());
            TPihakLain editedData = new TPihakLain();
            editedData = modelUtil.updateModelPihakLain(tPihakLain, uploadedBy);
            pihaklainRepository.save(editedData);
        } else {
            log.info("PihaklainService.processFile new data, filename: {}", file.getOriginalFilename());
            pihaklainRepository.save(modelUtil.createModelPihakLain(
                    this.createRequestDto(owner, uploadedBy, this.generateDir(owner), year, description),
                    file.getOriginalFilename(), file.getContentType(), year, description));
        }
        UploadResponseDto response = this.upload(file,
                this.createRequestDto(owner, uploadedBy, this.generateDir(owner), year, description));
        return new ApiResponse<>(HttpStatus.OK.value(), "File berhasil diupload", response);
    }

    private UploadRequestDto createRequestDto(String owner, String uploadedBy, String directory, Integer year,
            String description) {
        return new UploadRequestDto(UUID.randomUUID(), SeribuUploadConstant.PIHAK_LAIN, owner, directory, uploadedBy,
                year, description);
    }

    private String generateDir(String owner) {
        log.info("PihaklainService.generateDir owner name: [{}]", owner);
        String path = fileUploadConfig.getUploadDir() + SeribuUploadConstant.Directory.DIR_PIHAK_LAIN
                + SeribuUploadConstant.Directory.PATH_PREFIX + owner + SeribuUploadConstant.Directory.PATH_PREFIX
                + String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        return path;
    }

    @Override
    public DownloadResponseDto download(String fileName, String owner, int year) {
        log.info("PihaklainService.download [subditName: {}, filename: {}, year: {}]", owner, fileName, year);
        TPihakLain tPihakLain = pihaklainRepository.findByFileNameAndSubditYear(owner, fileName, year);
        log.info("path {}, filename: {}", tPihakLain.getPath(), tPihakLain.getFileName());
        return this.downloadFile(this.getPath(tPihakLain), tPihakLain.getFileType());
    }

    private String getPath(TPihakLain tPihakLain) {
        return tPihakLain.getPath() + SeribuUploadConstant.Directory.PATH_PREFIX + tPihakLain.getFileName();
    }

    @Override
    public List<UploadResponseDto> findAllActiveFile() {
        log.info("PihaklainService.findAllActiveFile");
        List<TPihakLain> listActiveFile = pihaklainRepository.findAllbyStatus();
        return listActiveFile.stream()
                .map(u -> new UploadResponseDto(u.getId(), SeribuUploadConstant.PIHAK_LAIN, u.getSubditName(),
                        u.getFileName(), u.getPath(), u.getFileType(), 0, String.valueOf(u.getReleaseYear()),
                        u.getDescription()))
                .collect(Collectors.toList());
    }

    @Override
    public int getTotalPihakLain() {
        log.info("PihaklainService.getTotalPihakLain");
        return pihaklainRepository.findAllbyStatus().size();
    }

    @Transactional
    @Override
    public boolean deleteDataAndFile(UploadRequestDto uploadRequestDto) {
        log.info("PihaklainService.deleteDataAndFile ID:{}", uploadRequestDto.getId());
        boolean deleted = false;
        Optional<TPihakLain> opPihakLain = pihaklainRepository.findById(uploadRequestDto.getId());
        pihaklainRepository.delete(opPihakLain.get());
        deleted = this.removeFile(opPihakLain.get().getPath().concat(SeribuUploadConstant.Directory.PATH_PREFIX).concat(opPihakLain.get().getFileName()));
        return deleted;
    }

}