package id.seribu.seribuupload.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribuupload.constant.SeribuUploadConstant;
import id.seribu.seribuupload.repository.SysParamRepository;
import id.seribu.seribuupload.service.SysParamService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SysParamServiceImpl implements SysParamService {

    @Autowired
    SysParamRepository sysParamRepository;

    @Override
    @Cacheable(value= SeribuUploadConstant.SYSPARAM, key="#groupParam", condition="#groupParam != null")
    public ApiResponse<List<Lov>> listSysParam(String groupParam) {
        log.info("SysParamService.listSysParam by group param [{}]", groupParam);
        return new ApiResponse<>(HttpStatus.OK.value(), "Data berhasil", sysParamRepository.findByGroupParam(groupParam)
            .stream().map(l -> new Lov(l.getName(), l.getValue())).collect(Collectors.toList()));
    }

}