package id.seribu.seribuupload.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.UploadRequestDto;
import id.seribu.seribudto.transaction.UploadResponseDto;
import id.seribu.seribuupload.exception.FileNotFoundException;
import id.seribu.seribuupload.exception.FileStorageException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractProcessFile {

    private Path path;

    protected void setPath(String archiveDir) {
        log.info("initiate directory AbstractProcessFile DIR: [{}]", archiveDir);
        this.path = Paths.get(archiveDir).toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.path);
            Files.setPosixFilePermissions(this.path, this.permissions());
        } catch (Exception ex) {
            throw new FileStorageException("Tidak dapat membuat folder direktori file.", ex);
        }
    }

    protected DownloadResponseDto downloadFile(String fileNameWithPath, String fileType) {
        log.info("AbstractProcessFile.downloadFile path: {}", fileNameWithPath);
        try {
            Path filePath = Paths.get(fileNameWithPath).toAbsolutePath().normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return new DownloadResponseDto(fileType, resource);
            } else {
                log.error("AbstractProcessFile.downloadFile error path:{}", fileNameWithPath);
                throw new FileNotFoundException("File tidak ditemukan " + fileNameWithPath);
            }
        } catch (MalformedURLException ex) {
            log.error("AbstractProcessFile.downloadFile error path:{}", fileNameWithPath);
            throw new FileNotFoundException("File tidak ditemukan " + fileNameWithPath, ex);
        }
    }

    protected UploadResponseDto upload(MultipartFile file, UploadRequestDto requestDto) {
        this.setPath(requestDto.getDirectory());
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        log.info("AbstractProcessFile.upload filename: {}, ", filename);
        try {
            if (filename.contains("..")) {
                throw new FileStorageException("Maaf, terdapat kesalahan pada nama file " + filename);
            }
            Path targetLocation = this.path.resolve(filename);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return new UploadResponseDto(UUID.randomUUID(), requestDto.getDataset(), requestDto.getOwner(), filename,
                    this.path.toString(), file.getContentType(), file.getSize(),
                    String.valueOf(requestDto.getReleaseYear()), requestDto.getDescription());
        } catch (IOException e) {
            log.error("AbstractProcessFile.upload fail ", e);
            throw new FileStorageException("Tidak dapat menyimpan file " + filename + ". Silakan coba lagi", e);
        }
    }

    private Set<PosixFilePermission> permissions() {
        log.info("AbstractProcessFile.permissions...");
        Set<PosixFilePermission> perm = new HashSet<>();
        perm.add(PosixFilePermission.OWNER_EXECUTE);
        perm.add(PosixFilePermission.OWNER_READ);
        perm.add(PosixFilePermission.OWNER_WRITE);
        perm.add(PosixFilePermission.GROUP_READ);
        perm.add(PosixFilePermission.GROUP_EXECUTE);
        perm.add(PosixFilePermission.GROUP_WRITE);
        perm.add(PosixFilePermission.OTHERS_READ);
        perm.add(PosixFilePermission.OTHERS_EXECUTE);
        perm.add(PosixFilePermission.OTHERS_WRITE);
        return perm;
    }

    protected boolean removeFile(String fileNameWithPath) {
        log.info("AbstractProcessFile.removeFile directory: {}", fileNameWithPath);
        Path filePath = Paths.get(fileNameWithPath).toAbsolutePath().normalize();
        File uploadedFile = filePath.toFile();
        if (uploadedFile.exists()) {
            return uploadedFile.delete();
        } else {
            // assume that no error happen, only file is not exist
            return true;
        }
    }

}