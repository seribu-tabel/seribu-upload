package id.seribu.seribuupload.constant;

public class SeribuUploadConstant {

    public static final String PIHAK_LAIN = "pihaklain";
    public static final String SUBDIT = "subdit";

    public static final String SYSPARAM = "SYSPARAM";

    public class Directory {
        public static final String DIR_PIHAK_LAIN = "/pihaklain";
        public static final String DIR_SUBDIT = "/subdit";
        public static final String PATH_PREFIX = "/";
    }
}