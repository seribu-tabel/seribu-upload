package id.seribu.seribuupload.constant;

public class SecurityConstant{
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String ROLES = "roles";
    public static final String INFO = "/actuator/**";
    public static final String REFRESH = "/refresh";
    public static final String HYSTRIX = "/hystrix.stream";


    public static final class HTTP {
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String JSON = "application/json";
        public static final String TIMESTAMP = "timestamp";
        public static final String STATUS = "status";
        public static final String ERROR = "error";
        public static final String EXCEPTION = "exception";

        private HTTP(){
            throw new UnsupportedOperationException();
        }
    }
}