package id.seribu.seribuupload.factory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.UploadRequestDto;
import id.seribu.seribudto.transaction.UploadResponseDto;
import id.seribu.seribuupload.constant.SeribuUploadConstant;
import id.seribu.seribuupload.service.PihaklainService;
import id.seribu.seribuupload.service.SubditService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UploadFactory {

    @Autowired
    SubditService subditService;

    @Autowired
    PihaklainService pihaklainService;

    @Transactional
    public ApiResponse<UploadResponseDto> processFile(String dataSet, String owner, String uploadedBy,
            MultipartFile file, int year, String description) {
        log.info("UploadFactory.processFile dataset: {}, owner: {}, uploadedBy: {}", dataSet, owner, uploadedBy);
        ApiResponse<UploadResponseDto> response = new ApiResponse<>();
        switch (dataSet) {
        case SeribuUploadConstant.SUBDIT:
            response = subditService.processFile(file, owner, uploadedBy, year, description);
            break;
        case SeribuUploadConstant.PIHAK_LAIN:
            response = pihaklainService.processFile(file, owner, uploadedBy, year, description);
            break;
        }
        return response;
    }

    @Transactional
    public DownloadResponseDto downloadFile(String dataSet, String owner, String fileName, int year) {
        DownloadResponseDto response = null;
        log.info("UploadFactory.downloadFile dataset: {}, owner: {}, filename: {}", dataSet, owner, fileName);
        switch (dataSet) {
        case SeribuUploadConstant.SUBDIT:
            log.info("subdit service invoke dataset:{}, constant:{}", dataSet, SeribuUploadConstant.SUBDIT);
            response = subditService.download(fileName, owner, year);
            break;
        case SeribuUploadConstant.PIHAK_LAIN:
            log.info("pihaklain service invoke dataset:{}, constant:{}", dataSet, SeribuUploadConstant.PIHAK_LAIN);
            response = pihaklainService.download(fileName, owner, year);
            break;
        }
        return response;
    }

    public List<UploadResponseDto> fetchActiveFile(String dataSet) {
        log.info("UploadFactory.fetchActiveFile dataset: {}", dataSet);
        List<UploadResponseDto> listResponse = new ArrayList<>();
        switch (dataSet) {
        case SeribuUploadConstant.SUBDIT:
            listResponse = subditService.findAllActiveFile();
            break;
        case SeribuUploadConstant.PIHAK_LAIN:
            listResponse = pihaklainService.findAllActiveFile();
            break;
        }
        return listResponse;
    }

    @Transactional
    public boolean deleteDataAndFile(UploadRequestDto uploadRequestDto) {
        boolean deleted = false;
        switch (uploadRequestDto.getDataset()) {
        case SeribuUploadConstant.SUBDIT:
            deleted = subditService.deleteDataAndFile(uploadRequestDto);
            break;
        case SeribuUploadConstant.PIHAK_LAIN:
            deleted = pihaklainService.deleteDataAndFile(uploadRequestDto);
            break;
        }
        return deleted;
    }

}