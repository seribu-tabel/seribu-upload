package id.seribu.seribuupload.filter;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import id.seribu.seribuupload.constant.SecurityConstant;
import id.seribu.seribuupload.constant.SecurityConstant.HTTP;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;

/**
 * @author arieki
 */
@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final String secret;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, final String secret) {
        super(authenticationManager);
        this.secret = secret;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain)
            throws IOException, ServletException{
        final String header = request.getHeader(SecurityConstant.HEADER_STRING);
        if(header == null || !header.startsWith(SecurityConstant.TOKEN_PREFIX)){
            chain.doFilter(request, response);
            return;
        }
        try{
            final UsernamePasswordAuthenticationToken authentication = this.getAuthentication(request);
            log.info("Authentication: {}", authentication);

            SecurityContextHolder.getContext().setAuthentication(authentication);
            chain.doFilter(request, response);
        } catch (final JwtException e){
            this.generateErrorResponse(response, e);
        }
    }
    
    private void generateErrorResponse(final HttpServletResponse response, final JwtException e) throws IOException {
        Throwable error = e;
        final int status = HttpServletResponse.SC_PRECONDITION_FAILED;
        response.setStatus(status);
        response.setHeader(HTTP.CONTENT_TYPE, HTTP.JSON);
        final Map<String, Object> errorAttributes = new LinkedHashMap<>();
        errorAttributes.put(HTTP.TIMESTAMP, new Date());
        errorAttributes.put(HTTP.STATUS, status);
        errorAttributes.put(HTTP.ERROR, HttpStatus.valueOf(status).getReasonPhrase());
        while (error.getCause() != null){
            error = error.getCause();
        }
        errorAttributes.put(HTTP.EXCEPTION, error.getClass().getName());
        response.getWriter().write(new ObjectMapper().writeValueAsString(errorAttributes));
    }

    private UsernamePasswordAuthenticationToken getAuthentication(final HttpServletRequest request){
        final String token = request.getHeader(SecurityConstant.HEADER_STRING);
        UsernamePasswordAuthenticationToken auth = null;
        if(token == null){
            return auth;
        }

        final Claims body = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token.replace(SecurityConstant.TOKEN_PREFIX, "")).getBody();
        final String user = body.getSubject();
        log.info("Caller ID: {} and jti: {}", user, body.getId());

        if(user != null){
            auth = new UsernamePasswordAuthenticationToken(user, null, this.getAuthority(body));
        }
        return auth;
    }

    @SuppressWarnings("unchecked")
    private Collection<? extends GrantedAuthority> getAuthority(final Claims body){
        final List<String> roles = (List<String>) body.get(SecurityConstant.ROLES);
        final Collection<GrantedAuthority> authorities = new HashSet<>();
        for(final String role : roles){
            authorities.add(() -> role);
        }
        return authorities;
    }
}