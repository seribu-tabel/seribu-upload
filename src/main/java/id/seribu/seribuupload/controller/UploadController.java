package id.seribu.seribuupload.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.transaction.DownloadResponseDto;
import id.seribu.seribudto.transaction.UploadRequestDto;
import id.seribu.seribudto.transaction.UploadResponseDto;
import id.seribu.seribuupload.factory.UploadFactory;
import id.seribu.seribuupload.service.PihaklainService;
import id.seribu.seribuupload.service.SubditService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class UploadController {

    @Autowired
    UploadFactory uploadFactory;

    @Autowired
    PihaklainService pihaklainService;

    @Autowired
    SubditService subditService;

    @PreAuthorize("hasRole('ROLE_SUPER') or hasRole('ROLE_ADMIN')")
    @PostMapping(path = "${application.endpoints.upload-service.process-file}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ApiResponse<UploadResponseDto> uploadFile(@RequestParam("file") MultipartFile file,
            @RequestParam("dataset") String dataSet, @RequestParam("owner") String owner,
            @RequestParam("releaseYear") Integer releaseYear, @RequestParam("description") String description,
            @RequestParam("uploadedBy") String uploadedBy) {
        log.info("UploadController.uploadFile...");
        return uploadFactory.processFile(dataSet, owner, uploadedBy, file, releaseYear, description);
    }

    @GetMapping(path = "${application.endpoints.upload-service.download-file}")
    public ResponseEntity<Resource> download(@PathVariable("dataset") String dataSet, @PathVariable String owner,
            @PathVariable String fileName, @PathVariable int year, HttpServletRequest request) {
        log.info("UploadController.download filename: [{}]", fileName);
        DownloadResponseDto responseDto = uploadFactory.downloadFile(dataSet, owner, fileName, year);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(responseDto.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + responseDto.getResource().getFilename() + "\"")
                .body(responseDto.getResource());
    }

    @GetMapping(path = "${application.endpoints.upload-service.fetch-all-file}")
    public ApiResponse<List<UploadResponseDto>> fetchAllActiveFile(@PathVariable("dataset") String dataSet) {
        log.info("UploadController.downloadActiveFile dataset: {}", dataSet);
        List<UploadResponseDto> listResponse = uploadFactory.fetchActiveFile(dataSet);
        return new ApiResponse<List<UploadResponseDto>>(HttpStatus.OK.value(), "data fetched", listResponse);
    }

    @GetMapping(path = "${application.endpoints.upload-service.get-total-subdit}")
    public ApiResponse<Integer> getTotalSubdit() {
        log.info("UploadController.getTotalSubdit");
        int totalData = subditService.getTotalSubdit();
        return new ApiResponse<>(HttpStatus.OK.value(), "total pivot data subdit", totalData);
    }

    @GetMapping(path = "${application.endpoints.upload-service.get-total-pihaklain}")
    public ApiResponse<Integer> getTotalPihakLain() {
        int totalData = pihaklainService.getTotalPihakLain();
        return new ApiResponse<>(HttpStatus.OK.value(), "total pivot data pihaklain", totalData);
    }

    @PreAuthorize("hasRole('ROLE_SUPER') or hasRole('ROLE_ADMIN')")
    @PostMapping(path = "${application.endpoints.upload-service.delete-data-file}"
        , consumes = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<Boolean> deleteUploadedFile(@RequestBody UploadRequestDto uploadRequestDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "delete success", uploadFactory.deleteDataAndFile(uploadRequestDto));
    }
}