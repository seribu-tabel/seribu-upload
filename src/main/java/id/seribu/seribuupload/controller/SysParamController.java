package id.seribu.seribuupload.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribuupload.service.SysParamService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class SysParamController {

    @Autowired
    SysParamService sysParamService;

    @GetMapping(path = "${application.endpoints.upload-service.get-list-sysparam}")
    public ApiResponse<List<Lov>> getListSysParam(@PathVariable("groupParam") String groupParam) {
        log.info("SysParamController.getListSysParam with group param [{}]", groupParam);
        return sysParamService.listSysParam(groupParam);
    }
}