package id.seribu.seribuupload.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import id.seribu.seribuupload.constant.SecurityConstant;
import id.seribu.seribuupload.filter.JwtAuthorizationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Value("${jwt.security.signature}")
    private String secret;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests().antMatchers(HttpMethod.GET, SecurityConstant.INFO).permitAll()
                .antMatchers(HttpMethod.POST, SecurityConstant.REFRESH).permitAll()
                .antMatchers(SecurityConstant.HYSTRIX).permitAll().anyRequest().authenticated().and()
                .addFilter(new JwtAuthorizationFilter(this.authenticationManager(), secret));
    }
}