package id.seribu.seribuupload.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TSubdit;

@Repository
public interface SubditRepository extends CrudRepository<TSubdit, UUID> {

        @Query(value = "SELECT distinct ts.id, sp.value as subdit_name, ts.path, ts.release_year, ts.description,"
                        + " ts.file_name, ts.file_type, ts.enabled, ts.uploaded_by, ts.created_date, ts.updated_date"
                        + " FROM t_subdit ts" + " join t_sys_param sp on sp.name = ts.subdit_name"
                        + " where enabled = true", nativeQuery = true)
        List<TSubdit> findAllbyStatus();

        @Query(value = "SELECT * from t_subdit where enabled = true and subdit_name = :subdit "
                        + "and file_name = :fileName and release_year = :releaseYear", nativeQuery = true)
        TSubdit findByFileNameAndSubditYear(@Param("subdit") String subdit, @Param("fileName") String fileName,
                        @Param("releaseYear") int releaseYear);

}