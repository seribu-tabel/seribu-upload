package id.seribu.seribuupload.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TSysParam;

@Repository
public interface SysParamRepository extends CrudRepository<TSysParam, String> {
    
    List<TSysParam> findByGroupParam(String groupParam);
}