package id.seribu.seribuupload.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.transaction.TPihakLain;

@Repository
public interface PihaklainRepository extends CrudRepository<TPihakLain, UUID> {

    @Query(value = "SELECT distinct pl.id, sp.value as subdit_name, pl.path, pl.release_year, pl.description,"
            + " pl.file_name, pl.file_type, pl.enabled, pl.uploaded_by, pl.created_date, pl.updated_date"
            + " FROM t_pihak_lain pl" + " join t_sys_param sp on sp.name = pl.subdit_name"
            + " where enabled = true", nativeQuery = true)
    List<TPihakLain> findAllbyStatus();

    @Query(value = "SELECT *FROM t_pihak_lain where enabled = true and subdit_name = :subdit "
            + "and file_name = :fileName and release_year = :releaseYear", nativeQuery = true)
    TPihakLain findByFileNameAndSubditYear(@Param("subdit") String subdit, @Param("fileName") String fileName,
            @Param("releaseYear") int releaseYear);
}