package id.seribu.seribuupload.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "file")
@EnableConfigurationProperties
@Getter @Setter
public class FileUploadConfig {

    private String uploadDir;
}