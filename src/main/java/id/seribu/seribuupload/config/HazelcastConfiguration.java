package id.seribu.seribuupload.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.seribu.seribuupload.constant.SeribuUploadConstant;

@Configuration
public class HazelcastConfiguration {

    @Value("${application.hazelcast.shortTtl}")
    private Integer timeToLive;
    @Value("${application.hazelcast.instancename}")
    private String hazelcastInstanceName;
    @Value("${application.hazelcast.maxsize}")
    private Integer maxSize;
    @Value("${application.hazelcast.groupname}")
    private String hazelcastGroupName;
    @Value("${application.hazelcast.grouppass}")
    private String hazelcastGroupPass;
    @Value("${application.hazelcast.portname}")
    private Integer portNumber;
    @Value("${application.hazelcast.members}")
    private String[] members;

    public HazelcastConfiguration() {

    }

    @Bean
    public Config hazelcastConfig() {
        final Config config = new Config();
        final NetworkConfig network = config.getNetworkConfig();
        network.setPort(this.portNumber);
        
        final JoinConfig join = network.getJoin();
        join.getMulticastConfig().setEnabled(false);
        join.getAwsConfig().setEnabled(false);
        final TcpIpConfig tcpIpConfig = join.getTcpIpConfig().setEnabled(true);
        for(final String member : members) {
            tcpIpConfig.addMember(new StringBuilder(member).append(":").append(portNumber).toString());
        }

        return config.setInstanceName(this.hazelcastInstanceName)
            .addMapConfig(new MapConfig().setName(SeribuUploadConstant.SYSPARAM)
                .setMaxSizeConfig(new MaxSizeConfig(maxSize, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
                .setEvictionPolicy(EvictionPolicy.LFU).setTimeToLiveSeconds(this.timeToLive))
            .setGroupConfig(this.createHzGroupConfig());
    }

    private GroupConfig createHzGroupConfig() {
        final GroupConfig groupConfig = new GroupConfig(this.hazelcastGroupName, this.hazelcastGroupPass);
        return groupConfig;
    }
}